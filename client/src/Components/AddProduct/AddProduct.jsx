import "./AddProduct.css";
import { React, useState } from 'react';
import Button from 'react-bootstrap/esm/Button';
import { Link } from 'react-router-dom';
import { nanoid } from 'nanoid';


import Book from '../Products/Book/Book';
import Cd from '../Products/Cd/Cd';
import Furniture from '../Products/Furniture/Furniture';

const AddProduct = ({ saveProduct }) => {

  const [product, setProduct ] = useState(
    {
      product_form : nanoid(),
      sku : "",
      name: "",
      price: "",
      type: "",
      size: "",
      height: "",
      width: "",
      length: "",
      weight: "",
    }
  );

  const handleChange = (event) => {
    setProduct(prevData => {
      return {...prevData, [event.target.name] : event.target.value}
    });    
    console.log(product)
  };
  
  return (
    <div className='add-product-container'>
      <div className='back-button'>
        <Link to="/">
          <Button>Back</Button>
        </Link>
      </div>
      <div className='add-product-form'>
        <div className='sku-input-container'>
          <label>SKU</label>
            <input
              placeholder={product.sku.length > 0 ? '' : 'Type in this field'}
              type="text"
              className={`sku-input ${product.sku.length > 0 ? "" : "invalid"}`}
              name="sku"
              onChange={handleChange}
            ></input>
        </div>

        <div className='name-input-container'>
          <label>Name</label>
            <input 
              placeholder={product.name.length > 0 ? '' : 'Type in this field'}
              type="text"
              className={`name-input ${product.name.length > 0 ? "" : "invalid"}`}
              name="name"
              onChange={handleChange}
            ></input>
        </div>

        <div className='price-input-container'>
          <label>Price</label>
            <input
              placeholder={product.price.length > 0 ? '' : 'Type in this field'}
              type="number"
              className={`price-input ${product.price.length > 0 ? "" : "invalid"}`}
              name="price"
              onChange={handleChange}
            ></input>
        </div>

        <div className='select-product-type'>
          <label>Type:</label>
          <select 
            name="type" 
            onChange={handleChange} 
            className="select-product-type"
          >
            <option value="">Choose</option>
            <option value="1">CD</option>
            <option value="2">Book</option>
            <option value="3">Furniture</option>
          </select>
        </div>

        <div>{product.type === "cd" ?
            <Cd handleChange={handleChange}/>
          : product.type === "book" ? 
            <Book handleChange={handleChange}/>
          : product.type === "furniture" ? 
            <Furniture handleChange={handleChange} />
          : "Please select the type of product to submit"}
        </div>
        <Button
          onClick={() => saveProduct(product)}
          variant="success"
        >
          Save
        </Button>
      </div>
      
    </div>
  )
}

export default AddProduct;