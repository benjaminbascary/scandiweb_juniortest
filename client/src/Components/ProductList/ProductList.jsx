import { React, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import './ProductList.css';
import { Product } from '../Product/Product';

const ProductList = () => {

  const [products, setProducts] = useState([{name: "banana", precio: 23}, {name: "manzana", precio: 18}])
  const [count, setCount] = useState(products.length)
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    return
    //llamar a la API y setProducts
    // setLoaded a true para que dezaparesca el mensaje.
  }, [count])



  return (
    <div>
      <div className='product-counter'>
        <p>
          {count? `Number of products: ${products.length}` : "There are no products to be desplayed."}
        </p>
      </div>
        <Link to="/addproduct" className='add-product-button'>
          <Button>Add product</Button>        
        </Link>
        <div className='all-products-container'>
          {products.map(forEach => {
            return <Product producto={forEach}/>
          })}
        </div>
    </div>
  )
}

export default ProductList;
