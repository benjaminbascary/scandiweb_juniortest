import React from 'react'
import Card from 'react-bootstrap/Card'

export const Product = ({ producto }) => {
  return (
    <Card>
        <p>Nombre: {producto.name}</p>
        <p>Precio: {producto.precio}</p>
    </Card>
  )
};

export default Product;
