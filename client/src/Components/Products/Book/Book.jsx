import React from 'react';

const Book = ({ handleChange }) => {
  return (
    <div>
      <label>Weight (gr)</label>
      <input
        name="weight"
        type="number"
        onChange={(event) => handleChange(event)}
      > 
      </input>
    </div>
  );
};

export default Book;
