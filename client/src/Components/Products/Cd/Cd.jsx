import React from 'react';

const Cd = ({ handleChange }) => {
  return (
    <div>
      <label>Size (mb)</label>
      <input
        name="size"
        type="number"
        onChange={(event) => handleChange(event)}
      > 
      </input>
    </div>
  );
};

export default Cd;