import React from 'react';
import './Furniture.css';

const Furniture = ({ handleChange }) => {
  return (
    <div className='furniture-form'>
      <div>
        <label>Height (CM)</label>
        <input
          name="height" 
          type="number"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>
      <div>
        <label>Width (CM)</label>
        <input 
          name="width"
          onChange={(event) => handleChange(event)}
          type="number"
        ></input>
      </div>
      <div>
        <label>Lenght (CM)</label>
        <input 
          name="length"
          type="number"
          onChange={(event) => handleChange(event)}
        ></input>
      </div>
    </div>
  )
};

export default Furniture;