import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AddProduct from './Components/AddProduct/AddProduct';
import { Header } from './Components/Header/Header';
import ProductList from './Components/ProductList/ProductList';

function App() {

  const saveProduct = (product) => {
    console.log(product)
  }


  return (
    
    <div className="App">
      <Router>
        <Header />
        <Routes>
          <Route exact path="/" element={ <ProductList /> } ></Route>
          <Route exact path="/addproduct" element= { <AddProduct saveProduct={saveProduct}/> }></Route>
        </Routes>
        
      </Router>
    </div>
  );
}

export default App;
